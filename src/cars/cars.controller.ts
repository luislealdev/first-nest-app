import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto';

@Controller('cars')
export class CarsController {
    constructor(private readonly carsService: CarsService) { }

    @Get()
    getAllCars() {
        return this.carsService.findAll();
    }

    @Get(':id')
    getCarById(@Param('id', new ParseUUIDPipe({ version: "4" })) id: string) {
        return this.carsService.findOneById(id);
    }

    @Post()
    // @UsePipes(ValidationPipe) // This is to use it just in one method
    createCar(@Body() createCarDto: CreateCarDto) {
        return this.carsService.create(createCarDto);
    }

    @Patch(':id')
    updateCar(@Body() body) {
        return body;
    }

    @Delete(':id')
    deleteCar(@Param('id', new ParseUUIDPipe({ version: "4" })) id: string) {
        return `Car ${id} deleted`;
    }
}