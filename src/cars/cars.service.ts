import { Injectable, NotFoundException } from '@nestjs/common';
import { Car } from './interfaces';
import { v4 as uuid } from 'uuid';
import { CreateCarDto } from './dto/create-car.dto';

@Injectable()
export class CarsService {
    private cars: Car[] = [
        {
            id: uuid(),
            model: 'Model 1',
            brand: 'Brand 1',
        },
        {
            id: uuid(),
            model: 'Model 2',
            brand: 'Brand 2',
        },
        {
            id: uuid(),
            model: 'Model 3',
            brand: 'Brand 3',
        },
    ];

    findAll() {
        return this.cars;
    }

    findOneById(id: string) {
        const car = this.cars.filter((car) => car.id === id);

        if (!car) throw new NotFoundException('ID car not found');

        return this.cars[id];
    }

    create(car: CreateCarDto) {
        const newCar = {
            ...car,
            id: uuid(),
        }

        this.cars.push(newCar);

        return newCar;
    }
}
